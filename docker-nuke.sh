#!/usr/bin/env bash
#
# scrippies/docker-nuke.sh

set -eou pipefail

# force-remove all (even running) containers
docker ps -aq | xargs docker rm -f || echo "meh"

# force-remove all (even tagged) images
docker images -aq | xargs docker rmi -f || echo "meh"

# force-remove all (even used) volumes (v1.9 and up)
docker volume ls -q | xargs docker volume rm || echo "meh"
