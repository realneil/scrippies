#!/bin/sh

# scrippies/youtube-dl-ogg

set -euvx

export LC_ALL=C

yt-dlp \
    --keep-video \
    --no-overwrites \
    --no-post-overwrites \
    --restrict-filenames \
    --write-subs \
    "$@"

exit "$?"
