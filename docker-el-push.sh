#!/usr/bin/env bash

# scrippies/docker-el-push.sh

set -euo pipefail

# [[ -z ${DOCKER_REGISTRY:-} ]] && \
#     { >&2 echo "error: undefined DOCKER_REGISTRY"; exit 1; }
# [[ -z ${DOCKER_USERNAME:-} ]] && \
#     { >&2 echo "error: undefined DOCKER_USERNAME"; exit 1; }
# [[ -z ${DOCKER_PASSWORD:-} ]] && \
#     { >&2 echo "error: undefined DOCKER_PASSWORD"; exit 1; }

export JOB_NAME=${JOB_NAME:-$(basename $(pwd))}

function _push()
{
    export OS_VERSION="${1}"
    export TAG_NAME="${JOB_NAME}:${OS_VERSION}"
    docker login \
           --username=${DOCKER_USERNAME} \
           --password=${DOCKER_PASSWORD} \
           ${DOCKER_REGISTRY}
    docker tag \
           --force \
           ${TAG_NAME} \
           ${DOCKER_REGISTRY}/${TAG_NAME}
    docker push \
           ${DOCKER_REGISTRY}/${TAG_NAME}
}

for OS_VERSION in 7 6 5
do
    _push ${OS_VERSION}
done
