#!/usr/bin/env bash

# scrippies/bitbucket-build-status.sh

# assuming...
#
#   * $BITBUCKET_CREDS holds your bitbucket username:(password|token)
#
#   * $USER_REPO is of the form user/project and references the git repository
#
# ...then query jenkins build status and write it to bitbucket

set -euo pipefail

[[ -n ${BITBUCKET_CREDS:-} ]] || \
    { >&2 echo "error: undefined env var BITBUCKET_CREDS"; exit 1; }

[[ 1 == $# ]] || \
    { >&2 echo "usage ${0} <INPROGRESS|SUCCESSFUL|FAILED>"; exit 1; }

export USER_REPO=$(git remote -v | head -n1 | egrep -o '[[:alnum:]_-]+/[[:alnum:]_-]+')

# jenkins mocking
export GIT_COMMIT=${GIT_COMMIT:-$(git rev-parse HEAD)}
export JOB_NAME=${JOB_NAME:-$(basename $(pwd))}
export BUILD_TAG=${BUILD_TAG:-"${JOB_NAME}-$(git describe --always --dirty)"}
export BUILD_URL=${BUILD_URL:-https://bitbucket.org/${USER_REPO}/commits/${GIT_COMMIT}}

curl \
    -sL \
    -w'\n' \
    -d grant_type=client_credentials \
    -u ${BITBUCKET_CREDS} \
    https://bitbucket.org/site/oauth2/access_token | \
    tr -d '{"}' | sed 's/, /\n/g' | grep access_token | awk '{print $NF}' | \
    xargs -I{} \
          curl \
          -sL \
          -w'\n' \
          -XPOST \
          -H "Authorization: Bearer {}" \
          -H "Content-Type: application/json" \
          -d '{"state":"'${1}'","key":"'${BUILD_TAG}'","url":"'${BUILD_URL}'"}' \
          https://api.bitbucket.org/2.0/repositories/${USER_REPO}/commit/${GIT_COMMIT}/statuses/build
