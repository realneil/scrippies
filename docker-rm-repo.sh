#!/usr/bin/env bash

set -eou pipefail

_this=$(readlink -f ${BASH_SOURCE})

_repo=${1}

shift

_args=${@}

for _tag in $(docker-ls tags ${_args} ${_repo} | sed -rn 's/^- "(.+)"$/\1/p')
do
    for _sha in $(docker-ls tag ${_args} ${_repo}:${_tag} | sed -rn 's/^- (sha256:.+)$/\1/p')
    do
        docker-rm ${_args} ${_repo}:${_sha}
    done
done
