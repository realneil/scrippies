#!/usr/bin/env bash

# scrippies/jenkins-trigger.sh

# assumes properly defined env vars...
#
#   * JENKINS_URL : url of the jenkins server
#   * JENKINS_API_USER : the user creating the job
#   * JENKINS_API_TOKEN : the token tied to JENKINS_API_USER
#   * JENKINS_JOB_NAME : the jenkins project name
#   * JENKINS_JOB_TOKEN : the trigger token tied to JENKINS_JOB_NAME
#
# ...and invokes curl to trigger a jenkins job

set -eu

curl \
    -v \
    -XPOST \
    --user "${JENKINS_API_USER}:${JENKINS_API_TOKEN}" \
    --data token="${JENKINS_JOB_TOKEN}" \
    ${JENKINS_URL}/job/${JENKINS_JOB_NAME}/buildWithParameters
