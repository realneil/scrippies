#!/usr/bin/env bash

# scrippies/docker-el-run.sh

# build images for the major versions of (some) Enterprise Linux

# given $PWD, a directory containing a Dockerfile with
#
#     FROM $DISTRO:$VERSION
#
# where
#
#     $DISTRO is one of centos, oraclelinux, rhel, etc
#
#     $VERSION is one of 5, 6, 7
#
# this script will (for $DISTRO tags 5, 6, 7) `docker build` an image and
# `docker tag` the result.

set -o errtrace
set -o nounset
set -o pipefail

trap onexit 1 2 3 15 ERR

onexit(){
    local exit_status=${1:-$?}
    >&2 echo " * * ** *** ***** *** ** * * "
    >&2 echo " * * ** *** ERROR *** ** * * "
    >&2 echo " * * ** *** ***** *** ** * * "
    >&2 date -uIs
    env | sort >&2
    >&2 echo Exiting ${0} with ${exit_status}
    exit ${exit_status}
}

_this=$(readlink -f ${BASH_SOURCE})
_here=$(dirname ${_this})

export JOB_NAME=${JOB_NAME:-$(basename $(pwd))}

${_here}/docker-clean.sh

for OS_VERSION in 7 6 5
do
    export OS_VERSION="${OS_VERSION}"
    export TAGNAME="${JOB_NAME}:${OS_VERSION}"
    docker run --rm -i ${TAGNAME} ${@:-"date -uIs"}
done

${_here}/docker-clean.sh
