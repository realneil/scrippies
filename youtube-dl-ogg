#!/bin/sh

# scrippies/youtube-dl-ogg

set -euvx

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

prio_log() (
    priority="$1"
    shift 1
    echo "$@" | logger --stderr --tag "${whatami}[$$]" --priority "${priority}" --
)

error() { prio_log user.error "ERROR:" "$@"; }
warning() { prio_log user.warning "WARNING:" "$@"; }
info() { prio_log user.info "INFO:" "$@"; }
die() {
    error "$@"
    exit 1
}

################################################################################
################################################################################
################################################################################

if ! cmd_yt_dlp="$(command -v yt-dlp)"; then
    die "missing command: yt-dlp"
fi
readonly cmd_yt_dlp="${cmd_yt_dlp}"

# 3.282 Portable Filename Character Set
# https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap03.html#tag_03_282
dump_single_json="$(printf '%s.json' "$*" | tr -sc '[:alnum:]._-' '_')"
readonly dump_single_json="${dump_single_json}"
info "dump_single_json:" "${dump_single_json}"
info "dumping single json..."
"${cmd_yt_dlp}" \
    --restrict-filenames \
    --dump-single-json \
    "$@" \
    >"${dump_single_json}"

info "${cmd_yt_dlp}" "$@"
"${cmd_yt_dlp}" \
    --audio-format vorbis \
    --extract-audio \
    --keep-video \
    --no-overwrites \
    --no-post-overwrites \
    --restrict-filenames \
    "$@"

exit "$?"
