#!/usr/bin/env bash

# scrippies/docker-build-run.sh

set -euo pipefail

_this=$(readlink -f ${BASH_SOURCE})
_here=$(dirname ${_this})

${_here}/docker-el-build.sh
${_here}/docker-el-run.sh ${@}

