#!/usr/bin/env bash
#
# scrippies/docker-clean.sh

set -eou pipefail

# remove exited containers
docker ps --no-trunc -aqf "status=exited" | xargs docker rm -f || echo "meh"

# remove dangling/untagged images
docker images --no-trunc -aqf "dangling=true" | xargs docker rmi -f || echo "meh"

# remove dangling volumes (v1.9 and up)
docker volume ls -qf "dangling=true" | xargs docker volume rm || echo "meh"
